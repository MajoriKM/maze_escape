﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Maze_Escape
{
    class Spike : Sprite
    {
        public Spike(Texture2D newTexture)
            : base(newTexture) //passes info up to parent class (Sprite)
        {

        }
    }
}
