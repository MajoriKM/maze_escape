﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;

namespace Maze_Escape
{
    class AnimatingSprite : Sprite
    {

        //-----------------------------------------
        // Types
        //-----------------------------------------
        struct Animation
        {
            public int startFrame;
            public int endFrame;
        }

        //-----------------------------------------
        // Data
        //-----------------------------------------

        // setting
        private int frameWidth;
        private int frameHieght;
        private Dictionary<string, Animation> animations = new Dictionary<string, Animation>();
        private float framesesPerSecond;
        

        // runtime
        private int currentFrame;
        private float timeInFrame;
        private string currentAnimation;
        private bool playing = false;

        //-----------------------------------------
        // Behavior
        //-----------------------------------------
        public AnimatingSprite(Texture2D newTexture, int newFrameWidth, int newFrameHieght, float newFPS) : base(newTexture) //passes info up to parent class (Sprite)
        {
            frameWidth = newFrameWidth;
            frameHieght = newFrameHieght;
            framesesPerSecond = newFPS;
        }

        public void AddAnimation(string name, int startFrame, int endFrame)
        {
            Animation newAnimation = new Animation();
            newAnimation.startFrame = startFrame;
            newAnimation.endFrame = endFrame;
            animations.Add(name, newAnimation);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (visible == true)
            {
                // draw animating sprite

                int numColumns = texture.Width / frameWidth;

                int xFrameIndex = currentFrame % numColumns; // remainder when divided
                int yFrameIndex = currentFrame / numColumns; // rounds down (ignores remainder)

                Rectangle source = new Rectangle(xFrameIndex * frameWidth, yFrameIndex * frameHieght, frameWidth, frameHieght);

                spriteBatch.Draw(texture, position, source, Color.White);

            }
        }

        public virtual void Update(GameTime gameTime)
        {

            // don't update if not playing
            if (!playing)
                return;

            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            timeInFrame += frameTime;

            // determine if time for new frame
            // calculate how long frame should last

            float timePerFrame = 1.0f / framesesPerSecond; // seconds per frame
            if (timeInFrame >= timePerFrame)
            {
                ++currentFrame;
                timeInFrame = 0;

                // determine whether to reset curent animation
                Animation thisAnimation = animations[currentAnimation];

                if (currentFrame > thisAnimation.endFrame)
                {
                    // reset animation back to begining frame
                    currentFrame = thisAnimation.startFrame;
                }
            }


        }

        public void playAnimation(string name)
        {

            if (name == currentAnimation && playing == true)
            {
                return;
            }
                


            // error check if animation is in dictionary
            bool animationIsSetup = animations.ContainsKey(name);
            Debug.Assert(animationIsSetup, "animation named "+ name + " not found in dictionary");

            if (animationIsSetup)
            {
                // record name of animation
                currentAnimation = name;
                // set current frame to animation
                currentFrame = animations[name].startFrame;
                // just reset
                timeInFrame = 0;
                // start animation
                playing = true;
            }

            
        }

        public void StopAnimation()
        {
            playing = false;

            // reset to start of animation
            currentFrame = animations[currentAnimation].startFrame;
        }

        public override Rectangle GetBounds()
        {
            return new Rectangle((int)position.X, (int)position.Y, frameWidth, frameHieght);
        }
    }
}

