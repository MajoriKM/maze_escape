﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Maze_Escape
{
    class Player : Actor// inherits from Actor
    {

        private int score = 0;
        private Level level;
        private int lives = 3;

        //-----------------------------------------
        // Constants
        //-----------------------------------------

        private const float MOVE_ACCEL = 10000;
        private const float MOVE_DRAG_FACTOR =  0.4f;
        private const float MAX_MOVE_SPEED = 500;
        private const float MIN_ANIM_SPEED = 10;

        //-----------------------------------------
        // Behavior
        //-----------------------------------------

        public Player(Texture2D newTexture, int newFrameWidth, int newFrameHieght, float newFPS, Level newlevel)
                : base(newTexture, newFrameWidth, newFrameHieght, newFPS)
        {
            AddAnimation("walkDown", 0, 3);
            AddAnimation("walkRight", 4, 7);
            AddAnimation("walkUp", 8, 11);
            AddAnimation("walkLeft", 12, 15);

            playAnimation("walkDown");

            level = newlevel;

        }
        public override void Update(GameTime gameTime)
        {
            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // get current keyboard state
            KeyboardState keyboardState = Keyboard.GetState();

            Vector2 movementInput = Vector2.Zero;

            if (keyboardState.IsKeyDown(Keys.A))
            {
                movementInput.X = -1.0f;
                playAnimation("walkLeft");
            }
            else if (keyboardState.IsKeyDown(Keys.D))
            {
                movementInput.X = 1.0f;
                playAnimation("walkRight");
            }
            else if (keyboardState.IsKeyDown(Keys.W))
            {
                movementInput.Y = -1.0f;
                playAnimation("walkUp");
            }
            else if (keyboardState.IsKeyDown(Keys.S))
            {
                movementInput.Y = 1.0f;
                playAnimation("walkDown");
            }

            // add movement change to velocity
            velocity += movementInput * MOVE_ACCEL * frameTime; // TODO remove magic number

            // apply drag from ground
            velocity *= MOVE_DRAG_FACTOR; // TODO remove magic number

            // if speed is too high give it a max
            if (velocity.Length() > MAX_MOVE_SPEED) // TODO remove magic number
            {
                velocity.Normalize();
                velocity *= MAX_MOVE_SPEED; // TODO remove magic number
            }

            //if player isn't moving stop animation
            if (velocity.Length() < MIN_ANIM_SPEED)
            {
                StopAnimation();
            }

            base.Update(gameTime);
        }

        public void HandleCollision(Wall hitWall)
        {
            // Determine collision depth with direction and magnitude
            // Tells us which direction to move

            Rectangle playerBounds = GetBounds();
            Vector2 depth = hitWall.GetCollisionDepth(playerBounds);

            if (depth != Vector2.Zero)
            {
                float absDepthX = Math.Abs(depth.X);
                float absDepthY = Math.Abs(depth.Y);

                if (absDepthY < absDepthX)
                {
                    position.Y = position.Y + depth.Y;
                }
                else
                {
                    position.X = position.X + depth.X;
                }
            }
        }
        public void HandleCollision(Coin hitCoin)
        {
            // collect coin
            // hide coin
            hitCoin.SetVisible(false);

            // add to score
            score += hitCoin.GetScore();
        }

        public void HandleCollision(Spike hitSpike)
        {
            kill();
        }

        public int GetScore()
        {
            return score;
        }

        public int GetLives()
        {
            return lives;
        }

        

        private void kill()
        {
            --lives;
            score = 0;
            //visible = false;
            level.ResetLevel();
        }

    }
           
}
