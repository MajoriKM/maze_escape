﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Maze_Escape
{
    class Actor : AnimatingSprite
    {

        //-----------------------------------------
        // Data
        //-----------------------------------------

        protected Vector2 velocity = new Vector2 (100,100);

        //-----------------------------------------
        // Behavior
        //-----------------------------------------
        public Actor(Texture2D newTexture, int newFrameWidth, int newFrameHieght, float newFPS) : base(newTexture, newFrameWidth, newFrameHieght, newFPS) //passes info up to parent class (Sprite)
        {

        }

        public override void Update(GameTime gameTime)
        {
            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            //update position based on frame time and velocity
            position += velocity * frameTime;

            //make sure animatingsprite still updates
            base.Update(gameTime);
        }


    }
}
