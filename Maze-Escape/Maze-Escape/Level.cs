﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Maze_Escape
{
    class Level
    {
        //-----------------------------------------
        // Data
        //-----------------------------------------
        Player player;
        List<Spike> spikes = new List<Spike>();
        Wall[,] walls;
        List<Coin> coins = new List<Coin>();
        int tileWidth;
        int tileHeight;
        int levelWidth;
        int levelHeight;
        Text scoreDisplay;
        Text liveDisplay;
        int currentLevel;
        bool reloadLevel = false;

        Texture2D playerSprite;
        Texture2D wallSprite;
        Texture2D coinSprite;
        Texture2D spikeSprite;
        SpriteFont UIFont;

        //-----------------------------------------
        // Behaviour
        //-----------------------------------------

        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            playerSprite = content.Load<Texture2D>("graphics/PlayerAnimation");
            wallSprite = content.Load<Texture2D>("graphics/Wall");
            coinSprite = content.Load<Texture2D>("graphics/Coin");
            spikeSprite = content.Load<Texture2D>("graphics/Spike");
            tileWidth = wallSprite.Width;
            tileHeight = wallSprite.Height;

            // setup UI
            UIFont = content.Load<SpriteFont>("fonts/mainFont");
            scoreDisplay = new Text(UIFont);
            scoreDisplay.SetPosition(new Vector2(10, 10));
            liveDisplay = new Text(UIFont);
            liveDisplay.SetPosition(new Vector2(graphics.Viewport.Bounds.Width - 10, 10));
            liveDisplay.SetAlignment(Text.Alignment.TOP_RIGHT);
            // create player and move later
            player = new Player(playerSprite, tileWidth, tileHeight, 6, this);

            // TEMP!!
            LoadLevel(1);

            
        }

        private void PositionPlayer(int tileX, int tileY)
        {  
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            player.SetPosition(tilePosition);
        }

        private void CreateWall(int tileX, int tileY)
        {
            Wall wall = new Wall(wallSprite);
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            wall.SetPosition(tilePosition);
            walls[tileX, tileY] = wall;
        }

        private void CreateCoin(int tileX, int tileY)
        {
            Coin coin = new Coin(coinSprite);
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            coin.SetPosition(tilePosition);
            coins.Add(coin);
        }

        private void CreateSpike(int tileX, int tileY)
        {
            Spike spike = new Spike(spikeSprite);
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            spike.SetPosition(tilePosition);
            spikes.Add(spike);
        }

        public void Update(GameTime gameTime)
        {
            //update all actors
            player.Update(gameTime);

            //collision checking
            List<Wall> collidingWalls = GetTilesInBounds(player.GetBounds());
            foreach (Wall collidingWall in collidingWalls)
            {
                player.HandleCollision(collidingWall);
            }

            // coin collisions
            foreach(Coin eachcoin in coins)
            {
                if (!reloadLevel && eachcoin.GetVisible() == true && eachcoin.GetBounds().Intersects(player.GetBounds()))
                {
                    player.HandleCollision(eachcoin);
                }
            }

            foreach(Spike eachSpike in spikes)
            {
                if (!reloadLevel && eachSpike.GetBounds().Intersects(player.GetBounds()))
                {
                    player.HandleCollision(eachSpike);
                }
            }
            

            // update score
            scoreDisplay.SetTextString("score " + player.GetScore());

            liveDisplay.SetTextString("Lives " + player.GetLives());


            // check if need to reload level
            if(reloadLevel == true)
            {
                LoadLevel(currentLevel);
                reloadLevel = false;
            }
        }

        public List<Wall> GetTilesInBounds(Rectangle bounds)
        {
            List<Wall> tilesInBounds = new List<Wall>();

            // determine tile ccord range for the rect
            int leftTile = (int)Math.Floor((float)bounds.Left / tileWidth);
            int rightTile = (int)Math.Ceiling((float)bounds.Right / tileWidth) - 1;
            int topTile = (int)Math.Floor((float)bounds.Top / tileHeight);
            int bottumTile = (int)Math.Ceiling((float)bounds.Bottom / tileHeight) - 1;

            //loop through range and add tiles to list
            for (int x = leftTile; x <= rightTile; ++x)
            {
                for (int y = topTile; y <= bottumTile; ++y)
                {
                    // only add tile if exist, not null and visible
                    Wall thisTile = GetTile(x, y);

                    if (thisTile != null && thisTile.GetVisible() == true)
                        tilesInBounds.Add(thisTile);
                }
            }

            return tilesInBounds;
        }

        public Wall GetTile(int x, int y)
        {
            if (x < 0 || x >= levelWidth || y < 0 || y >= levelHeight)
                return null;

            return walls[x, y];
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            player.Draw(spriteBatch);

            foreach(Spike eachspike in spikes)
            {
                eachspike.Draw(spriteBatch);
            }
            

            foreach(Wall eachwall in walls)
            {
                if(eachwall != null)
                    eachwall.Draw(spriteBatch);
            }
            foreach(Coin eachcoin in coins)
            {
                eachcoin.Draw(spriteBatch);
            }

            scoreDisplay.Draw(spriteBatch);
            liveDisplay.Draw(spriteBatch);
        }

        public void LoadLevel(int levelNum)
        {
            currentLevel = levelNum;
            string baseLevelName = "Levels/Level_";
            LoadLevel(baseLevelName + levelNum.ToString() + ".txt");
        }

        public void LoadLevel(string fileName)
        {

            ClearLevel();

            // crate filestream to open file and ready for reading
            Stream fileStream = TitleContainer.OpenStream(fileName);

            // before reading in the tiles for level. need to know the size of the level overall
            int lineWidth = 0; // will become levelWidth
            int numLines = 0; // will become levelHeight
            List<string> lines = new List<string>(); // this will contain all strings of text in the file
            StreamReader reader = new StreamReader(fileStream); // this will let us read each line from the file
            string line = reader.ReadLine(); // Gets the first line from the file
            lineWidth = line.Length; // assume the overall line width is the same as the first line
            while(line != null) // for as long as line exists
            {
                lines.Add(line); // add current line to list
                if(line.Length != lineWidth)
                {
                    // This means the lines are different sizes and causes problems
                    throw new Exception("line are different widths - error occured on line " + lines.Count);
                }

                // read the next line to be ready for next step in the loop
                line = reader.ReadLine();
            }

            // we have read into all lines in the file into the list
            // we can now know how many lines there are
            numLines = lines.Count;

            // Now we can set up the tile array
            levelWidth = lineWidth;
            levelHeight = numLines;
            walls = new Wall[levelWidth, levelHeight];

            // loop over every tile position and check the letter ther and place a tile
            for(int y = 0; y < levelHeight; ++y)
            {
                for(int x = 0; x < levelWidth; ++x)
                {
                    // load ech tile
                    char tileType = lines[y][x];
                    // TODO: load the tile
                    LoadTile(tileType, x, y);
                }
            }

            // verify if the level is playable
            if(player == null)
            {
                throw new NotSupportedException("A level must have a starting point for the player.");
            }

        }

        private void LoadTile(char tileType, int tileX, int tileY)
        {
            switch (tileType)
            {
                // player
                case 'P':
                    PositionPlayer(tileX, tileY);
                    break;

                // wall
                case 'W':
                    CreateWall(tileX, tileY);
                    break;

                // coin
                case 'C':
                    CreateCoin(tileX, tileY);
                    break;

                // spike
                case 'S':
                    CreateSpike(tileX, tileY);
                    break;

                // blank spance
                case '.':
                    break;
                
                // any non - hanfled symbol
                default:
                    throw new NotSupportedException("Level contained unsupported symbol " + tileType + " at line ");
                    

                
            }


        }
        public void ResetLevel()
        {
            // delay reloading level after update loop
            reloadLevel = true;
            
        }

        private void ClearLevel()
        {
            spikes.Clear();
            coins.Clear();
            
        }

    }
}
