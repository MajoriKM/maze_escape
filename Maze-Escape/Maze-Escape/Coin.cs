﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Maze_Escape
{
    class Coin : Sprite
    {

        private int scoreValue = 100;

        public Coin(Texture2D newTexture)
            : base(newTexture) //passes info up to parent class (Sprite)
        {

        }

        public int GetScore()
        {
            return scoreValue;
        }

    }
}
