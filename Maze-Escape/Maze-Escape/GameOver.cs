﻿using System;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Maze_Escape
{
    class GameOver
    {
        // Data
        Text message;

        // behaviour
        public void LoadContent(ContentManager content, GraphicsDevice device)
        {
            // create message
            SpriteFont tempFont = content.Load<SpriteFont>("fonts/largeFont");

            message = new Text(tempFont);
            message.SetTextString("GAME OVER");
            message.SetPosition(new Vector2(device.Viewport.Bounds.Width / 2));
            message.SetAlignment(Text.Alignment.CENTRE);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            message.Draw(spriteBatch);
        }

    }
}
