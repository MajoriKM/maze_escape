﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Maze_Escape
{
    class Wall : Sprite
    {
        public Wall(Texture2D newTexture) 
            : base(newTexture) //passes info up to parent class (Sprite)
        {
            
        }

        public Vector2 GetCollisionDepth (Rectangle otherBounds)
        {
            // this calculates how far our rectangles are overlapping
            Rectangle tileBounds = GetBounds();

            //calculate half sizes of both rects
            float halfWidthPlayer = otherBounds.Width / 2;
            float halfHeightPlayer = otherBounds.Height / 2;
            float halfWidthTile = tileBounds.Width / 2;
            float halfHeightTile = tileBounds.Height / 2;

            Vector2 centrePlayer = new Vector2(otherBounds.Left + halfWidthPlayer, 
                otherBounds.Top + halfHeightPlayer);

            Vector2 centreTile = new Vector2(tileBounds.Left + halfWidthTile, 
                tileBounds.Top + halfHeightTile);

            //how far apart are the centres of the rect from eachother
            float distanceX = centrePlayer.X - centreTile.X;
            float distanceY = centrePlayer.Y - centreTile.Y;

            //min distance these need to not collide
            float minDistanceX = halfWidthPlayer + halfWidthTile;
            float minDistanceY = halfHeightPlayer + halfHeightTile;

            // If we are not intersecting return (0,0)
            if (Math.Abs(distanceX) >= minDistanceX || Math.Abs(distanceY) >= minDistanceY)
            {
                return Vector2.Zero;
            }

            // calculate and return intersection depth
            float depthX, depthY;

            if (distanceX > 0)
                depthX = minDistanceX - distanceX;
            else
                depthX = -minDistanceX - distanceX;
            if (distanceY > 0)
                depthY = minDistanceY - distanceY;
            else
                depthY = -minDistanceY - distanceY;

            return new Vector2(depthX, depthY);
        }
    }
}
